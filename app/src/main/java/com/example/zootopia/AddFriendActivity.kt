package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zootopia.items.GenericItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_add_friend.*

class AddFriendActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val currentUID = FirebaseAuth.getInstance().uid ?: ""
    private lateinit var friends: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_friend)
        supportActionBar?.title = "Znajdź swoich znajomych"

        addfriend_search.setOnClickListener {
            search()
        }
    }


    private fun search() {
        val adapter = GroupAdapter<GroupieViewHolder>()
        val text = addfriend_textedit.text.toString()
        db.collection("users").document(currentUID).get()
            .addOnSuccessListener {
                friends = it.toString()
                db.collection("users").get()
                    .addOnSuccessListener { users ->
                        users.forEach { user ->
                            val username = user.get("username").toString()
                            val uid = user.id
                            if (username.contains(text, true) && !friends.contains(uid)) {
                                val path = user.get("photo").toString()
                                if (uid != currentUID) adapter.add(
                                    (GenericItem(
                                        username,
                                        path,
                                        uid
                                    ))
                                )
                            }
                        }

                        adapter.setOnItemClickListener { item, view ->
                            val userItem = item as GenericItem
                            val intent = Intent(view.context, UserWallActivity::class.java)
                            intent.putExtra("USER_UID", userItem.uid)
                            startActivity(intent)
                        }

                        addfriend_recyclerview.adapter = adapter
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, it.message.toString(), Toast.LENGTH_LONG).show()
                    }
            }
            .addOnFailureListener {
                friends = ""
            }

    }
}

