package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.zootopia.items.OpinionItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_comments.*
import kotlinx.android.synthetic.main.alertdialog_delete.view.*

class CommentsActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    lateinit var currentUID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)
        currentUID = FirebaseAuth.getInstance().uid ?: ""
        val userUID = intent.getStringExtra("USER_UID")
        fetchOpinions(userUID)

    }

    private fun fetchOpinions(userUID: String) {
        val adapter = GroupAdapter<GroupieViewHolder>()
        val zoosRef = db.collection("zoos")
        var flag = true

        adapter.setOnItemClickListener { item, view ->
            val opinionItem = item as OpinionItem
            val intent = Intent(
                view.context,
                ZooWallActivity::class.java
            )
            intent.putExtra("ZOO_UID", opinionItem.uid)
            startActivity(intent)
        }

        if (userUID == currentUID) {
            adapter.setOnItemLongClickListener { item, view ->
                val opinionItem = item as OpinionItem
                val customView = layoutInflater.inflate(R.layout.alertdialog_delete, null)
                val builder = AlertDialog.Builder(this)
                    .setView(customView)
                val alertDialog = builder.show()
                customView.delete_cancel.setOnClickListener {
                    alertDialog.dismiss()
                }
                customView.delete_ok.setOnClickListener {
                    alertDialog.dismiss()
                    val opinionRef =
                        db.collection("zoos").document(opinionItem.uid).collection("opinions")
                            .document(userUID)
                    opinionRef.delete()
                    adapter.remove(item)
                    adapter.notifyDataSetChanged()

                }



                return@setOnItemLongClickListener true
            }
        }

        zoosRef.get()
            .addOnSuccessListener { zoos ->
                zoos.forEach { zoo ->
                    val name = zoo.get("name").toString()
                    val path = zoo.get("photo").toString()
                    val zooUID = zoo.id
                    val opinionRef = db.collection("zoos").document(zooUID).collection("opinions")
                    opinionRef.get()
                        .addOnSuccessListener opinions@{ opinions ->
                            opinions.forEach { opinion ->
                                Log.d("Ooga", "")
                                val opinionUID = opinion.id
                                if (opinionUID == userUID) {
                                    Log.d("Ooga", "")
                                    val comment = opinion.get("comment").toString()
                                    val rating = opinion.get("rating").toString().toInt()
                                    adapter.add(OpinionItem(name, path, comment, rating, zooUID))
                                    return@opinions
                                }
                            }

                        }
                        .addOnFailureListener {

                        }
                }
                Log.d("Ooga", "")
                comments_recyclerview.adapter = adapter
            }
            .addOnFailureListener {

            }

    }

}
