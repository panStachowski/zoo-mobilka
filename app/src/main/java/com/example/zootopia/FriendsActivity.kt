package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zootopia.items.EmptyItem
import com.example.zootopia.items.GenericItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_friends.*

class FriendsActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    private val currentUID = FirebaseAuth.getInstance().uid ?: ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)
        supportActionBar?.title = "Znajomi"
        fetchFriends()
    }

    private fun fetchFriends() {
        val adapter = GroupAdapter<GroupieViewHolder>()
        val userRef = db.collection("users").document(currentUID)
        userRef.get()
            .addOnSuccessListener { friendlist ->
                val temp = friendlist.get("friends").toString()
                if (temp != "[]" && temp != "null" && temp != "") {
                    val friends = friendlist.get("friends") as List<String>
                    friends.forEach {
                        db.collection("users").document(it).get()
                            .addOnSuccessListener { user ->
                                val username = user.get("username").toString()
                                val path = user.get("photo").toString()
                                val uid = user.id
                                adapter.add(GenericItem(username, path, uid))
                            }
                            .addOnFailureListener {
                                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                            }
                    }
                    adapter.setOnItemClickListener { item, view ->
                        val friendItem = item as GenericItem
                        val intent = Intent(view.context, UserWallActivity::class.java)
                        intent.putExtra("USER_UID", friendItem.uid)
                        startActivity(intent)
                    }
                } else {
                    adapter.add(EmptyItem())
                }
                friends_recyclerview.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
            }
    }

}

