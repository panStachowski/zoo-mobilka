package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zootopia.items.EmptyItem
import com.example.zootopia.items.GenericItem
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_visited_zoos.*

class VisitedZoosActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    lateinit var UID: String
    lateinit var visited: List<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visited_zoos)
        UID = intent.getStringExtra("USER_UID")
        fetchVisited()
    }

    private fun fetchVisited() {
        val adapter = GroupAdapter<GroupieViewHolder>()
        db.collection("users").document(UID).get()
            .addOnSuccessListener { user ->
                val temp = user.get("visited_zoos").toString()
                if (temp != "" && temp != "[]" && temp != "null") {
                    visited = user.get("visited_zoos") as List<String>
                    if (visited.toString() != "[null]") {
                        visited.forEach { zoos ->
                            db.collection("zoos").document(zoos).get()
                                .addOnSuccessListener { zoo ->
                                    val name = zoo.get("name").toString()
                                    val path = zoo.get("photo").toString()
                                    val uid = zoo.id
                                    adapter.add(GenericItem(name, path, uid))
                                }
                                .addOnFailureListener {
                                    Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                                }
                        }
                        adapter.setOnItemClickListener { item, view ->
                            val zooItem = item as GenericItem
                            val intent = Intent(view.context, ZooWallActivity::class.java)
                            intent.putExtra("ZOO_UID", zooItem.uid)
                            startActivity(intent)
                        }
                    }
                } else {
                    adapter.add(EmptyItem())
                }
                visitedzoos_recyclerview.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
            }
    }
}