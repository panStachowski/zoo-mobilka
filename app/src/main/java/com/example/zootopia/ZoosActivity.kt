package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore

class ZoosActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    val db = FirebaseFirestore.getInstance()
    val center = LatLng(52.112795, 19.211946)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoos)
        supportActionBar?.title = "Mapa Zoo"
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun fetchZoos(mMap: GoogleMap) {
        db.collection("zoos").get()
            .addOnSuccessListener { zoos ->

                zoos.forEach { zoo ->
                    if (zoo.getGeoPoint("coords") != null) {
                        val uid = zoo.id
                        val title = zoo.get("name").toString()
                        val position = zoo.getGeoPoint("coords")
                        val lat = position?.latitude ?: 0.0
                        val lng = position?.longitude ?: 0.0
                        mMap.addMarker(MarkerOptions().position(LatLng(lat, lng)).title(title))
                            .apply {
                                this.tag = uid
                            }
                    }
                }

            }

            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                val intent = Intent(this@ZoosActivity, ZooWallActivity::class.java)
                intent.putExtra("ZOO_UID", marker.tag.toString())
                startActivity(intent)
                return true
            }
        })

        fetchZoos(mMap)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(center))
        mMap.moveCamera(CameraUpdateFactory.zoomTo(5.65f))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_titles, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {

            R.id.nav_titles -> {
                val intent = Intent(this, ZooListActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
