package com.example.zootopia

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zootopia.items.EmptyItem
import com.example.zootopia.items.OpinionItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_opinions.*

class OpinionsActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    lateinit var currentUserUID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opinions)
        val zooUID = intent.getStringExtra("ZOO_UID")
        currentUserUID = FirebaseAuth.getInstance().uid ?: ""

        db.collection("zoos").document(zooUID).get()
            .addOnSuccessListener {
                supportActionBar?.title = it.get("name").toString() + " - opinie"
            }
        fetchOpinions(zooUID)
    }

    private fun fetchOpinions(zooUID: String) {
        val adapter = GroupAdapter<GroupieViewHolder>()
        var userUID: String
        val opinionsRef = db.collection("zoos").document(zooUID).collection("opinions")
        opinionsRef.get()
            .addOnSuccessListener { opinions ->
                val temp = opinions.toString()
                if (temp != "" && temp != "[]" && temp != "null") {
                    opinions.forEach { opinion ->
                        val rating = opinion.get("rating").toString().toInt()
                        val comment = opinion.get("comment").toString()
                        userUID = opinion.id
                        db.collection("users").document(userUID).get()
                            .addOnSuccessListener {
                                val username = it.get("username").toString()
                                val path = it.get("photo").toString()
                                adapter.add(OpinionItem(username, path, comment, rating, zooUID))

                            }
                            .addOnFailureListener {
                                Toast.makeText(this, "${it.message.toString()}", Toast.LENGTH_LONG)
                                    .show()
                            }
                    }
                } else {
                    adapter.add(EmptyItem())
                }
                opinions_recyclerview.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message.toString()}", Toast.LENGTH_LONG).show()
            }
    }

}

