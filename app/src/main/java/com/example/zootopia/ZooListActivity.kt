package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zootopia.items.GenericItem
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_zoo_list.*

class ZooListActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    val zooList = mutableListOf<Map<String, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoo_list)
        fetchZoos()
    }

    private fun fetchZoos() {
        val adapter = GroupAdapter<GroupieViewHolder>()
        val zoosRef = db.collection("zoos")
        zoosRef.get()
            .addOnSuccessListener { zoos ->
                zoos.forEach { zoo ->
                    val name = zoo.get("name").toString()
                    val path = zoo.get("photo").toString()
                    val uid = zoo.id
                    val map = mapOf("name" to name, "path" to path, "uid" to uid)
//                    adapter.add(GenericItem(name, path, uid))
                    zooList.add(map)
                }
                zooList.sortBy {it.get("name")}
                zooList.forEach { zoo ->
                    adapter.add(GenericItem(zoo.get("name").toString(), zoo.get("path").toString(), zoo.get("uid").toString()))
                }

                adapter.setOnItemClickListener { item, view ->
                    val zooItem = item as GenericItem
                    val intent = Intent(view.context, ZooWallActivity::class.java)
                    intent.putExtra("ZOO_UID", zooItem.uid)
                    startActivity(intent)
                }
                zoolist_recyclerview.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
            }


    }
}
