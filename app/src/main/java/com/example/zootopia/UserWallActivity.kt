package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_wall.*


class UserWallActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    var flag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_wall)
        val userUID = intent.getStringExtra("USER_UID")
        val currentUID = FirebaseAuth.getInstance().uid ?: ""
        getUsername(userUID)
        getImage(userUID)
        checkIfFriends(currentUID, userUID)

        userwall_add.setOnClickListener {
            performAddOrRm(currentUID, userUID)
        }

        userwall_opinions.setOnClickListener {
            val intent = Intent(this, CommentsActivity::class.java)
            intent.putExtra("USER_UID", userUID)
            startActivity(intent)
        }

        userwall_visited.setOnClickListener {
            val intent = Intent(this, VisitedZoosActivity::class.java)
            intent.putExtra("USER_UID", userUID)
            startActivity(intent)
        }
    }

    private fun performAddOrRm(uid: String, UID: String) {

        userwall_add.isEnabled = false
        userwall_add.isClickable = false
        if (flag) {
            db.collection("users").document(uid).update(
                "friends", FieldValue.arrayRemove(UID)
            )
                .addOnSuccessListener {
                    userwall_add.isEnabled = true
                    userwall_add.isClickable = true
                    userwall_add.text = "Dodaj do znajomych"
                    flag = false
                }
                .addOnFailureListener {
                    Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                }
        } else {
            db.collection("users").document(uid).update(
                "friends", FieldValue.arrayUnion(UID)
            )
                .addOnSuccessListener {
                    userwall_add.isEnabled = true
                    userwall_add.isClickable = true
                    userwall_add.text = "Usuń ze znajomych"
                    flag = true
                }
                .addOnFailureListener {
                    Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                }
        }
    }

    private fun checkIfFriends(uid: String, UID: String) {
        db.collection("users").document(uid).get()
            .addOnSuccessListener { users ->
                val temp = users.get("friends").toString()
                if (temp != "" && temp != "[]" && temp != "null") {
                    val friends = users.get("friends") as List<String>
                    friends.forEach { friend ->
                        if (friend == UID) {
                            userwall_add.text = "Usuń ze znajomych"
                            flag = true
                        }
                    }
                }
            }
    }

    private fun getUsername(UID: String) {
        db.collection("users").document(UID).get()
            .addOnSuccessListener {
                val username: String = it.get("username").toString()
                if (username != "" && username != "null") userwall_username.text = username
                else userwall_username.text = "Brak nazwy użytkownika"
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                userwall_username.text = "Brak nazwy użytkownika"
            }

    }

    private fun getImage(UID: String) {
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        db.collection("users").document(UID).get()
            .addOnSuccessListener {
                val path = it.get("photo").toString()
                if (path != "" && path != "null") {
                    storageRef.child(path).downloadUrl
                        .addOnSuccessListener {
                            Picasso.get().load(it).into(userwall_photo)
                        }
                        .addOnFailureListener {
                            Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                        }
                } else userwall_photo_text.text = "Brak zdjęcia"
            }
    }
}
