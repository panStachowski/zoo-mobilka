package com.example.zootopia

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_zoo_wall.*
import kotlinx.android.synthetic.main.alertdialog_comment.view.*

class ZooWallActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    private lateinit var currentUID: String
    private lateinit var zooUID: String
    private var flag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoo_wall)
        zooUID = intent.getStringExtra("ZOO_UID")
        currentUID = FirebaseAuth.getInstance().uid ?: ""
        getZooData(zooUID)
        getImage(zooUID)
        fetchRating(zooUID)
        checkIfVisited(currentUID, zooUID)

        zoowall_opinions.setOnClickListener {
            val intent = Intent(this, OpinionsActivity::class.java)
            intent.putExtra("ZOO_UID", zooUID)
            startActivity(intent)
        }

        zoowall_add.setOnClickListener {
            performAddOrRm(currentUID, zooUID)
        }

        zoowall_featured.setOnClickListener {
            featured(zooUID)
        }

        zoowall_addopinion.setOnClickListener {
            var rating = 0
            var customView = layoutInflater.inflate(R.layout.alertdialog_comment, null)
            val builder = AlertDialog.Builder(this)
                .setView(customView)
            val alertDialog = builder.show()
            val opinionsRef =
                db.collection("zoos").document(zooUID).collection("opinions").document(currentUID)
            opinionsRef.get()
                .addOnSuccessListener { opinion ->
                    var temp = opinion.data.toString()
                    if (temp != "" && temp != "null" && temp != "[]") {
                        customView.alert_comment.setText(opinion.get("comment").toString())
                        rating = opinion.get("rating").toString().toInt()
                        when (rating) {
                            1 -> {
                                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
                            }
                            2 -> {
                                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
                            }
                            3 -> {
                                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
                            }
                            4 -> {
                                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
                            }
                            5 -> {
                                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_on)
                                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_on)
                            }
                            else -> {
                                rating = 0
                                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_off)
                                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
                            }
                        }
                    }
                }
                .addOnFailureListener {

                }

            customView.alertdialog_cancel.setOnClickListener {
                alertDialog.dismiss()
            }
            customView.alertdialog_ok.setOnClickListener {
                val temp = customView.alert_comment.text.toString()
                if (rating == 0) {
                    Toast.makeText(this, "Zapomniałeś o ocenie", Toast.LENGTH_LONG).show()
                } else if (temp == "" || temp == "null") {
                    Toast.makeText(this, "Komentarz nie może być pusty", Toast.LENGTH_LONG).show()
                } else {
                    alertDialog.dismiss()
                    val comment = customView.alert_comment.text.toString()
                    addComment(rating, comment, currentUID, zooUID)
                }

            }
            customView.alert_rating1_btn.setOnClickListener {
                rating = 1
                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_off)
                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_off)
                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_off)
                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
            }
            customView.alert_rating2_btn.setOnClickListener {
                rating = 2
                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_off)
                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_off)
                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
            }
            customView.alert_rating3_btn.setOnClickListener {
                rating = 3
                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_off)
                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
            }
            customView.alert_rating4_btn.setOnClickListener {
                rating = 4
                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_off)
            }
            customView.alert_rating5_btn.setOnClickListener {
                rating = 5
                customView.alert_rating1_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating2_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating3_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating4_img.setImageResource(android.R.drawable.btn_star_big_on)
                customView.alert_rating5_img.setImageResource(android.R.drawable.btn_star_big_on)
            }
        }
    }

    private fun fetchRating(zooUID: String) {
        val opinionsRef = db.collection("zoos").document(zooUID).collection("opinions")
        opinionsRef.get()
            .addOnSuccessListener { opinions ->
                var rating = 0.0
                var counter = 0.0
                if (opinions.toString() != "null" && opinions.toString() != "[]") {
                    opinions.forEach { opinion ->
                        counter++
                        rating += opinion.get("rating").toString().toInt()
                    }
                }
                zoowall_counter.text = "(" + counter.toInt().toString() + ")"
                var average = rating.div(counter)
                when (average) {
                    in 1.0..1.5 -> {
                        zoowall_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating2.setImageResource(android.R.drawable.btn_star_big_off)
                        zoowall_rating3.setImageResource(android.R.drawable.btn_star_big_off)
                        zoowall_rating4.setImageResource(android.R.drawable.btn_star_big_off)
                        zoowall_rating5.setImageResource(android.R.drawable.btn_star_big_off)
                    }
                    in 1.5..2.5 -> {
                        zoowall_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating3.setImageResource(android.R.drawable.btn_star_big_off)
                        zoowall_rating4.setImageResource(android.R.drawable.btn_star_big_off)
                        zoowall_rating5.setImageResource(android.R.drawable.btn_star_big_off)
                    }
                    in 2.5..3.5 -> {
                        zoowall_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating3.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating4.setImageResource(android.R.drawable.btn_star_big_off)
                        zoowall_rating5.setImageResource(android.R.drawable.btn_star_big_off)
                    }
                    in 3.5..4.5 -> {
                        zoowall_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating3.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating4.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating5.setImageResource(android.R.drawable.btn_star_big_off)
                    }
                    in 4.5..5.0 -> {
                        zoowall_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating3.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating4.setImageResource(android.R.drawable.btn_star_big_on)
                        zoowall_rating5.setImageResource(android.R.drawable.btn_star_big_on)
                    }
                    else -> {
                    }
                }
            }
    }

    private fun addComment(rating: Int, comment: String, userUID: String, zooUID: String) {
        val opinionsRef = db.collection("zoos").document(zooUID).collection("opinions")
        val opinion = mapOf<String, Any>(
            "comment" to comment,
            "rating" to rating
        )
        opinionsRef.document(currentUID).set(opinion)
            .addOnSuccessListener {
                fetchRating(zooUID)
            }
    }

    private fun featured(UID: String) {
        val intent = Intent(this, FeaturedActivity::class.java)
        intent.putExtra("ZOO_UID", UID)
        startActivity(intent)
    }

    private fun performAddOrRm(uid: String, UID: String) {
        zoowall_add.isEnabled = false
        zoowall_add.isClickable = false
        if (flag) {
            db.collection("users").document(uid).update(
                "visited_zoos", FieldValue.arrayRemove(UID)
            )
                .addOnSuccessListener {
                    zoowall_add.isEnabled = true
                    zoowall_add.isClickable = true
                    zoowall_add.text = "Dodaj do odwiedzonych"
                    flag = false
                }
                .addOnFailureListener {
                    Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                }
        } else {
            db.collection("users").document(uid).update(
                "visited_zoos", FieldValue.arrayUnion(UID)
            )
                .addOnSuccessListener {
                    zoowall_add.isEnabled = true
                    zoowall_add.isClickable = true
                    zoowall_add.text = "Usuń z odwiedzonych"
                    flag = true
                }
                .addOnFailureListener {
                    Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                }
        }
    }

    private fun checkIfVisited(uid: String, UID: String) {
        db.collection("users").document(uid).get()
            .addOnSuccessListener { user ->
                val temp = user.get("visited_zoos").toString()
                if (temp != "" && temp != "[]" && temp != "null") {
                    val visitedZoos = user.get("visited_zoos") as List<String>
                    visitedZoos.forEach { zoo ->
                        if (zoo == UID) {
                            zoowall_add.text = "Usuń z odwiedzonych"
                            flag = true
                        }
                    }
                }
            }
    }

    private fun getImage(UID: String) {
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        db.collection("zoos").document(UID).get()
            .addOnSuccessListener { zoo ->
                val path = zoo.get("photo").toString()
                if (path != "" && path != "null") {
                    storageRef.child(path).downloadUrl
                        .addOnSuccessListener {
                            Picasso.get().load(it).into(zoowall_photo)
                        }
                        .addOnFailureListener {
                            zoowall_photo_text.text = "Brak zdjęcia"
                        }
                }
            }
    }

    private fun getZooData(UID: String) {
        db.collection("zoos").document(UID).get()
            .addOnSuccessListener {
                val name: String = it.get("name").toString()
                val address: String = it.get("address").toString()
                if (name != "" && name != "null") zoowall_name.text = name
                else zoowall_name.text = "Brak nazwy zoo"
                if (address != "" && address != "null") zoowall_address.text = address
                else zoowall_address.text = "Brak adresu zoo"
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                zoowall_name.text = "Brak nazwy zoo"
                zoowall_address.text = "Brak adresu zoo"
            }
    }
}
