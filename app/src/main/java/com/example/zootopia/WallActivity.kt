package com.example.zootopia

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_wall.*
import kotlinx.android.synthetic.main.alertdialog_username.view.*
import java.util.*

class WallActivity : AppCompatActivity() {

    var username = ""
    var path = ""
    var selectedPhotoUri: Uri? = null
    val db = FirebaseFirestore.getInstance()
    var currentUID = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wall)
        verifyUserIsLoggedIn()


        wall_opinions.setOnClickListener {
            val intent = Intent(this, CommentsActivity::class.java)
            intent.putExtra("USER_UID", currentUID)
            startActivity(intent)
        }

        wall_photo_btn.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        wall_visited.setOnClickListener {
            val intent = Intent(this, VisitedZoosActivity::class.java)
            intent.putExtra("USER_UID", currentUID)
            startActivity(intent)
        }

        wall_findzoo.setOnClickListener {
            val intent = Intent(this, ZoosActivity::class.java)
            startActivity(intent)
        }

        wall_name.setOnClickListener {
            val customView = layoutInflater.inflate(R.layout.alertdialog_username, null)
            val builder = AlertDialog.Builder(this)
                .setView(customView)
            val alertDialog = builder.show()
            customView.alertdialog_cancel.setOnClickListener {
                alertDialog.dismiss()
            }
            customView.alertdialog_ok.setOnClickListener {
                alertDialog.dismiss()
                username = customView.alertdialog_edittext.text.toString()
                updateUsername()
            }
        }

        wall_friends.setOnClickListener {
            val intent = Intent(this, FriendsActivity::class.java)
            startActivity(intent)
        }

        wall_addfriends.setOnClickListener {
            val intent = Intent(this, AddFriendActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            selectedPhotoUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)
            wall_photo_imgview.setImageBitmap(bitmap)
            wall_photo_btn.alpha = 0f
            uploadImageToFirebase()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_nav, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.nav_logout -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun uploadImageToFirebase() {
        if (selectedPhotoUri == null) return
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/users/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    path = "/users/$filename"
                    updatePhotoPath()
                }

            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    private fun updatePhotoPath() {
        val mappath: Map<String, String> = mapOf(
            "photo" to path
        )

        db.collection("users").document(currentUID).update(mappath)
            .addOnSuccessListener {
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    private fun updateUsername() {
        val mapusername: Map<String, String> = mapOf(
            "username" to username
        )
        db.collection("users").document(currentUID).update(mapusername)
            .addOnSuccessListener {
                if (username != "") wall_name.text = username
                else wall_name.text = "Kliknij aby dodać swoją nazwę użytkownika"
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    private fun getUsername(currentUID: String) {
        db.collection("users").document(currentUID).get()
            .addOnSuccessListener {
                val username: String = it.get("username").toString()
                if (username != "" && username != "null") wall_name.text = username
                else wall_name.text = "Kliknij aby dodać swoją nazwę użytkownika"
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                wall_name.text = "Kliknij aby dodać swoją nazwę użytkownika"
            }
    }

    private fun getImage(currentUID: String, path: String) {
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        db.collection("users").document(currentUID).get()
            .addOnSuccessListener {
                val path = it.get("photo").toString()
                if (path != "" && path != "null") {
                    wall_photo_btn.alpha = 0f
                    storageRef.child(path).downloadUrl
                        .addOnSuccessListener {
                            Picasso.get().load(it).into(wall_photo_imgview)
                        }
                        .addOnFailureListener {
                            Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                            wall_photo_btn.alpha = 1f
                        }
                }
            }
    }

    private fun verifyUserIsLoggedIn() {
        var UID = FirebaseAuth.getInstance().uid
        if (UID == null) {
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        } else {
            currentUID = UID
            getUsername(UID)
            getImage(UID, path)
        }
    }


}