package com.example.zootopia.items

import com.example.zootopia.R
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.groupie_featured.view.*

class FeatureItem(private val feature: String, private val index: Int): Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
       viewHolder.itemView.featured_number.text = "#" + (index+1).toString()
       viewHolder.itemView.featured_featured.text = feature
    }

    override fun getLayout(): Int {
        return R.layout.groupie_featured
    }
}