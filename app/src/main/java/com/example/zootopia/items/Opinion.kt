package com.example.zootopia.items

import com.example.zootopia.R
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.groupie_comment.view.*

class OpinionItem(val name: String, val path: String, val comment: String, val rating: Int?, val uid: String): Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        if(name=="") viewHolder.itemView.comment_name.text = "Brak nazwy użytkownika"
        else viewHolder.itemView.comment_name.text = name
        viewHolder.itemView.comment_comment.text = comment
        when(rating){
            1 -> {
                viewHolder.itemView.comment_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating2.setImageResource(android.R.drawable.btn_star_big_off)
                viewHolder.itemView.comment_rating3.setImageResource(android.R.drawable.btn_star_big_off)
                viewHolder.itemView.comment_rating4.setImageResource(android.R.drawable.btn_star_big_off)
                viewHolder.itemView.comment_rating5.setImageResource(android.R.drawable.btn_star_big_off)
            }
            2 -> {
                viewHolder.itemView.comment_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating3.setImageResource(android.R.drawable.btn_star_big_off)
                viewHolder.itemView.comment_rating4.setImageResource(android.R.drawable.btn_star_big_off)
                viewHolder.itemView.comment_rating5.setImageResource(android.R.drawable.btn_star_big_off)
            }
            3 -> {
                viewHolder.itemView.comment_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating3.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating4.setImageResource(android.R.drawable.btn_star_big_off)
                viewHolder.itemView.comment_rating5.setImageResource(android.R.drawable.btn_star_big_off)
            }
            4 -> {
                viewHolder.itemView.comment_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating3.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating4.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating5.setImageResource(android.R.drawable.btn_star_big_off)
            }
            5 -> {
                viewHolder.itemView.comment_rating1.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating2.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating3.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating4.setImageResource(android.R.drawable.btn_star_big_on)
                viewHolder.itemView.comment_rating5.setImageResource(android.R.drawable.btn_star_big_on)
            }
            else -> {}
        }
        val storageRef = FirebaseStorage.getInstance().getReference()
        storageRef.child(path).downloadUrl
            .addOnSuccessListener {
                Picasso.get().load(it).into(viewHolder.itemView.comment_photo)
            }
            .addOnFailureListener {
                viewHolder.itemView.comment_photo_txt.text = "Brak nazwy użytkownika"
            }
    }

    override fun getLayout(): Int {
        return R.layout.groupie_comment
    }
}