package com.example.zootopia.items

import com.example.zootopia.R
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class EmptyItem: Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {

    }
    override fun getLayout(): Int {
        return R.layout.groupie_empty
    }
}