package com.example.zootopia.items

import com.example.zootopia.R
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.groupie_item.view.*

class GenericItem(private val name: String, private val path: String, val uid: String): Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        if(name=="") viewHolder.itemView.groupie_name.text = "Brak nazwy użytkownika"
        else viewHolder.itemView.groupie_name.text = name
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        if(path!="null"){
            storageRef.child(path).downloadUrl
                .addOnSuccessListener {
                    Picasso.get().load(it).into(viewHolder.itemView.groupie_photo)
                }
                .addOnFailureListener{
                    viewHolder.itemView.groupie_photo_txt.text = "Brak zdjęcia"
                }
        }
        else {
            viewHolder.itemView.groupie_photo_txt.text = "Brak zdjęcia"
        }

    }

    override fun getLayout(): Int {
        return R.layout.groupie_item
    }
}
