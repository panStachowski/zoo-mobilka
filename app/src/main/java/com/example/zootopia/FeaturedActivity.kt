package com.example.zootopia

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zootopia.items.EmptyItem
import com.example.zootopia.items.FeatureItem
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_featured.*

class FeaturedActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_featured)
        val zooUID = intent.getStringExtra("ZOO_UID")
        fetchFeatured(zooUID)
    }

    private fun fetchFeatured(zooUID: String) {
        val adapter = GroupAdapter<GroupieViewHolder>()
        db.collection("zoos").document(zooUID).get()
            .addOnSuccessListener {
                val temp = it.get("featured").toString()
                if (temp != "" && temp != "[]" && temp != "null") {
                    val featured = it.get("featured") as List<String>
                    featured.forEachIndexed { index, feature ->
                        adapter.add(FeatureItem(feature, index))

                    }
                } else {
                    adapter.add(EmptyItem())
                }
                featured_recyclerview.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()
            }

    }


}


